# generated from catkin/cmake/template/pkg.context.pc.in
CATKIN_PACKAGE_PREFIX = ""
PROJECT_PKG_CONFIG_INCLUDE_DIRS = "/home/nvidia/catkin_ws/devel/include;/home/nvidia/catkin_ws/src/avt_vimba_camera/include".split(';') if "/home/nvidia/catkin_ws/devel/include;/home/nvidia/catkin_ws/src/avt_vimba_camera/include" != "" else []
PROJECT_CATKIN_DEPENDS = "camera_info_manager;diagnostic_updater;dynamic_reconfigure;image_geometry;image_transport;roscpp;sensor_msgs;std_msgs;polled_camera".replace(';', ' ')
PKG_CONFIG_LIBRARIES_WITH_PREFIX = "".split(';') if "" != "" else []
PROJECT_NAME = "avt_vimba_camera"
PROJECT_SPACE_DIR = "/home/nvidia/catkin_ws/devel"
PROJECT_VERSION = "0.0.10"
